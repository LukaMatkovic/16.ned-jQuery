$(document).ready(function(){

    //ucitavanje podataka o zanrovima i filmovima iz JSON-a

    $.getJSON('db.json', function(response){

    var genres = response.genres;
    var movies = response.movies;
    var $movies = $('#movies');

    var movieArray = [];

    //kreiranje liste zanrova

        $.each(genres,function(index, value) {
            $('<li class="link"><a href="#!'+value+'" class="genreLink" data-genre="'+value+'">'+value+'</a></li>').appendTo('#genres');
            });

        $('li.link').fadeIn(1000);

    //kreiranje liste filmova prilikom klika na odredjeni zanr

        $('#genres').on('click','a',function(e) {
            $('#movies').empty();
            movieArray = [];

            var genreClick = e.target.dataset.genre;
            console.log(genreClick);
            for(let i=0; i<movies.length; i++) {
                for (let j=0; j<movies[i].genres.length; j++) {
                    if(movies[i].genres[j] == genreClick) {
                            movieArray.push(movies[i]);
                    }
                }
            };

            $.each(movieArray,function(key, value) {
                console.log(this);

                $(`<div class="movie" data-id="${this.id}">
                <center><img src="${this.posterUrl}" width="300px" height="auto" alt="poster"></center>
                <h3><center>${this.title} (${this.year})</center></h3></div>`).appendTo('#movies');
            });
        });

    //detaljniji prikaz podataka o filmu klikom na njega

        console.log(movieArray);
        $('#movies').on('click','.movie', function (e) {
            $('aside, main, header').fadeTo(500, 0.1).fadeTo(1000, 0.9).fadeTo(1500, 0.2);
            $('#movieWindow').empty();
            var movieId = e.currentTarget.dataset.id;
            var movieClick = movieArray.filter(function(movie) {
                    return movie.id == movieId;
            })
            console.log(movieClick);
            var target = e.target;
            while (target.parentNode !== $movies && movieId === null) {
                movieId = target.getAttribute('data-id');
                target = target.parentNode;
            }
            if (movieId !== null) {
                $('#movieWindow').css('display','block');
                $('#shut').css('display','block');

                $('<h3><center>'+movieClick[0].title+' ('+movieClick[0].year+')</center></h3>'+
                '<center><img src="'+movieClick[0].posterUrl+'" width="250px" height="auto" alt="poster"></center>'+
                '<p><strong>Runtime: </strong>'+movieClick[0].runtime+' minutes</p>'+
                '<p><strong>Genre: </strong>'+movieClick[0].genres+'</p>'+
                '<p><strong>Director: </strong>'+movieClick[0].director+'</p>'+
                '<p><strong>Actors: </strong>'+movieClick[0].actors+'</p>'+
                '<p><strong>Plot: </strong>'+movieClick[0].plot+'</p>').appendTo('#movieWindow');
            }
        });

    //zatvaranje pop-up kontejnera

        $('#shut').on('click', function () {
            $('#movieWindow').slideUp(1500);
            $('#shut').slideUp();
            $('aside, main, header').fadeTo(1500, 1);

        });

    //renderovanje random movie slika u headeru

        console.log(movies);
        var imgArray = movies.map(movie=>movie.posterUrl);

        setInterval(function(){
            $('#i1').empty();
            $('#i2').empty();
            $('#i3').empty();
            $('#i4').empty();
            $('#i5').empty();

            var randomImg1 = imgArray[Math.floor(Math.random()*imgArray.length)];
            var randomImg2 = imgArray[Math.floor(Math.random()*imgArray.length)];
            var randomImg3 = imgArray[Math.floor(Math.random()*imgArray.length)];
            var randomImg4 = imgArray[Math.floor(Math.random()*imgArray.length)];
            var randomImg5 = imgArray[Math.floor(Math.random()*imgArray.length)];

            $('<img src="'+randomImg1+'" alt="random photo" width="200px" height="300px">').appendTo('#i1');
            $('<img src="'+randomImg2+'" alt="random photo" width="200px" height="300px">').appendTo('#i2');
            $('<img src="'+randomImg3+'" alt="random photo" width="200px" height="300px">').appendTo('#i3');
            $('<img src="'+randomImg4+'" alt="random photo" width="200px" height="300px">').appendTo('#i4');
            $('<img src="'+randomImg5+'" alt="random photo" width="200px" height="300px">').appendTo('#i5');

            },1200);
        })


    //dodavanje animacija

        $('#genres')
        .on('mouseenter', '.genreLink', function (e) {
           $(e.currentTarget).addClass('hoveredGenre');
        })
        .on('mouseleave', '.genreLink', function (e) {
            $(e.currentTarget).removeClass('hoveredGenre');
        });

        $('#login').on('click',function (){
            $('#loginform, #formsubmit').slideDown(1000);
            $('#login').slideUp(1000);
        });
        $('#formsubmit').on('click', function(){
            $('#loginform, #formsubmit').slideUp(1000);
            $('header').prepend('<p id="loginMessage">You are now logged in!</p>');
            $('#logout').slideDown(1000);
        });
        $('#logout').on('click', function(){
            $('#logout').slideUp(1000);
            $('#login').slideDown(1000);
            $('#loginMessage').remove();

        })
});