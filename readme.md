16.nedelja - jQuery
===================

Dopunjen prethodni domaći zadatak (Učitavanje filmova iz JSON fajla):
---------------------------------------------------------------------
- svim elementima je manipulisano preko jQuery biblioteke u index.js
- JSON fajl je učitan preko jQuery-ja (izbačen axios)
- izbačen Mustache template i korigovane uočene greške iz prethodnog domaćeg zadatka
- dodate basic animacije (fadeTo, slideUp)
- kreirana basic login forma i dodate animacije prilikom klika na login, submit i logout
- animacija: random prikazivanje slika u headeru-u

Potrebno još odraditi:
----------------------
- animirati pojavljivanje filmova na promenu zanra
- dodati favourites 
- preko baze podataka dati mogućnost da user čuva favourites
